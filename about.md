---
layout: page
title: About
permalink: /about/
published: true
---

# About
My name is Brandon Giesing and I am a "zany to the max" teenager who helps people with technology while Bill Clinton plays the sax.[^1]

Now if you didn't get any of that you obviously don't know me and you should continue reading this short biography!

[^1]: Bill Clinton is not really going to play the sax while I teach you tech.

## Tech

Before we explain those jokes, let's talk about my number one interest: Technology!

Okay, let's throw in another joke: You could call me a tech maniac but call me that and you die!

So what do you want to know first about the wide world of Tech?

### Operating Systems and Software
Let's start with my favorite Operating System ( or OS )? Well it depends on the device you are using.

For desktop, my favorite operating system is Windows 10 or Arch Linux.
On mobile, I love Android; however, I used to own an iPod touch so I don't mind using iOS also!

My favorite thing to do in my free time is to mess around with new software for Android and my favorite type is keyboards. Now why would you want to replace the keyboard that comes on your phone with another one? Well, it may be able to speed up your typing! My favorites are SwiftKey and Fleksy as they can speed up your typing dramatically.

### Netcasts
To learn about Tech, I watch online shows called "Netcasts".[^2]

Some of the popular ones include:

* This Week in Tech
* Tekzilla
* Tech News Today
* All About Android

[^2]: They are more popularly known as Podcasts.

### Gadgets
Want to know about the gadgets I have in my hammerspace?

#### Mobile Gadgets
* Google Nexus 7 (2012/2013)
* Motorola Moto G (2013)
* Pocket Juice MAX
* Philips GOGEAR Vibe MP3 Player

#### Computers
* TOSHIBA C55t Laptop
* eMachines EL1333G-03w Desktop
* Samsung Series 5
* Google CR-48 Prototype

#### Accessories
* Logitech K400 Keyboard with built-in Trackpad
* Apple Wireless Bluetooth Keyboard
* SMK-Link Bluetooth Mouse

#### TV
* Google Chromecast
* Vizio VIA TV (Powered by Yahoo)

## Music
I also like a lot of music. I will listen to most genres out there and I used to play the Clarinet.

### Favorite Genres
* Adult Contemporary
* Rock
* Dance
* Techno
* Dubstep
* Drum and Bass

### Hated Genres
* Country (some songs are okay)
* Reggae
* Salsa/Samba
* Folk
* Spiritual
* African music

## Animation
Time to explain the jokes! You may have noticed above that there are a lot of TV Show References and most of them are from Animations.

One of them that you may not heard of is the hammerspace thing I was talking about earlier. Well, it is a concept that behind the character's back (or in a bag) is an invisible access point where they can hide something that only they can access.  Have you ever seen a cartoon character pull a mallet or pie from behind their backs to hit a bad guy with? That is their hammerspace!

One of my favorite cartoons of all-time is Animaniacs! This show had a lot of great humor (if not risky at some times), great music, and great animation combining to bring an all in one slap happy comedy joy ride!
