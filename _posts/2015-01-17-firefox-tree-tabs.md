---
layout: post
title: "Testing Firefox with Tree Tabs"
published: true
tags:
  - Firefox
  - Tabs
  - Addons
quote:  I'm a Chrome Die-Hard, but today I am trying out Firefox with Tree Style Tabs. Will this make me switch?﻿
comments: true
---

I'm a Chrome Die-Hard, but today I am trying out Firefox with Tree Style Tabs. This may make me switch!

![Firefox with Addons](http://i.imgur.com/HS4ykf8.png)

## What is Tree Style Tabs?
[Tree Style Tabs][1] moves the Tabs to a sidebar and they are organized like a folder.

It allows better productivity!

## Other Addons
I also have some other addons installed including:

- [AdBlock Plus][2]: I use this mostly for annoying sites but I let though some!
- [HTTPS Everywhere][3]: This is a must-have privacy tool!
- [DuckDuckGo Plus][4]: Makes DuckDuckGo Better and even if you use Google, it will spruce it up with DDG's answers.
- [Tab Mix Plus][5]: Enhance's Firefox and is compatiable with Tree Style Tabs!
- [Lazarus][6]: Make sure you don't loose forms due to crashes!
- [Session Manager][7]: Save tabs (and Tree Style Tab's Tree Structure) across sessions!
- [Download Manager (S3)][8]: Get rid of the Download Window and move it to a bar at the bottom like Chrome
- [FT DeepDark Theme][9]: Awesome Dark Theme

## Where is the Title Bar?
Also, you may notice that I don't have a Title bar.

This is because a Linux distro I just installed called [Manjaro Cup of Linux Edition (or mCOLe)][10] with the Borderless option enabled This looks cleaner and cooler when maximized and allows better window tiling!

[1]: https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab/
[2]: https://adblockplus.org/en/
[3]: https://www.eff.org/https-everywhere
[4]: http://ddg.gg
[5]: http://tmp.garyr.net/
[6]: http://getlazarus.com/
[7]: http://sessionmanager.mozdev.org/
[8]: http://www.s3blog.org/
[9]: https://addons.mozilla.org/en-US/firefox/addon/ft-deepdark/
[10]: http://cupoflinux.com
