---
layout: post
title: Learning VIM
published: true
category:
    - Technology
tags:
    - VIM
image:
    url: https://camo.githubusercontent.com/fcb4a5209477dea6cb80c25505b1f74ee83f7945/687474703a2f2f66696c6573312e7765646f6973742e636f6d2f65393532666462333433623165363137623930643235366534373464303337302f61732f73637265656e73686f745f312e706e67
    source: https://github.com/amix/vimrc
color: 696969
comments: true
quote: "This week, I learned VIM! Here's my progress!"
---

So this past week, I have been trying to learn something new. That new thing you may have heard of is called VIM, but if you haven't I will explain below!

## What's VIM (or Vi)?
VIM (or Vi IMproved) is a text editor for basically every operating system out there including Windows, Mac, Linux, OS/2, BSD, and tons more including even mobile devices like Android even though that would be stupid. Why? Well because VIM is keyboard-driven! Everything is done with keyboard shortcuts and no mouse at all. Heck you don't even use the arrow keys to move because they want you to be more efficient!

## Why would you want to learn this?
Simply put, **It makes you more efficient**! Let's walk though a few examples and you will see why!

## Examples
First, say you have a piece of text that looks like this:

~~~
## LET'S DO IT!
    For today's entry, we have a simple setup by me based on [Manjaro Cup of Linux Edition (or mCOLe)][mCOLe] with a few tweaks!
    ![mCOLe](/bgiesing-site-2015/media/2015-01-18-hss-mCOLe/mCOLe-hss.png)
~~~

Maybe you want to replace `[Manjaro Cup of Linux Edition (or mCOLe)]` with something else but first you are still on the first line. This is simple to do and will be way more efficient than using Notepad or other text editors.

### Steps
1. From the first line press `j` to move down a line.
2. Press `f[` to "find" the first `[`
3. Press `ci[` to "Change Inside Square Bracket", this will delete the text and allow you to replace it with something new.
4. Press `Esc` to exit Insert mode to return to editing.

Isn't that simple? It seems a lot of work at first, but you can type it in a few seconds compared to fiddling with the mouse for 2 minutes trying to select only the items you want, then deleting them, then replacing them.

## Modes? What are those?
You may have noticed that I mentioned something named Insert mode. This is why VIM (and Vi) are so different (and so confusing) to most people. Think of Insert mode as what you have now with Notepad or basically **any** other text editor. Normal mode is what you are in when you start VIM and this allows you to navigate text like we did above. There are more modes, but they are not used anywhere near as often.

## Where can I try it?
First, download it at [Vim's Site][1], then run the vimtutor[^1], then I recommend that you watch [these videos by Derek Wyatt][2] to enhance your skills.

[^1]: Run `vimtutor` from the terminal on Linux/Mac or on Windows run `Vim tutor` from the Start Menu.
[1]: http://vim.org
[2]: http://derekwyatt.org/vim/tutorials/
