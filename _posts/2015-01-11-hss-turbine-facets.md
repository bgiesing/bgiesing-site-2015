---
layout: post
title: "Home Screen Sunday: Turbine Facets"
published: true
category:
  - Home Screen Sunday
tags:
  - Facets
  - Moto G
  - Action Launcher
image:
  url: /bgiesing-site-2015/media/2015-01-15-hss-turbine-facets/cover.png
  source: "http://google.com/+BrandonGiesing"
video: false
comments: true
redirect_from: /hss-turbine-facets/
color: 28E1B5
quote: For our first HSS, we have my current Android 5.0 Lollipop Home Screen on my Moto G.
---

{{ site.data.hss.intro }}

## LET'S DO IT!
First up, we have my current Android 5.0 Lollipop Home Screen on my Moto G.

![Turbine Facets](/bgiesing-site-2015/media/2015-01-15-hss-turbine-facets/cover.png)

### APP REQUIREMENTS
To achieve this setup, you will need these apps:

- [Facets] for the Wallpaper
- [Squatch Icons]
- [Action Launcher], Need Plus Version for Icons

### SETUP
Assuming that you downloaded everything, let's set it up.

#### ACTION LAUNCHER
* Go to `Settings > Display > Icon pack > Squatch > OK` to choose the Icon Pack.
* `Settings > Layout`
    * Top control set to `Action Bar`
    * Dock set to `4`
* 4 folders above dock (Place most used app first, open folder and turn into cover by clicking overflow menu.)
    * Games
    * Fit
    * Discover
    * Create
* Dock (last 3, turn into Covers like above)
    * Browser/Chrome
    * Social
    * Play
    * Photos

#### FACETS
1. Launch the App
2. Scroll down to `January 2014`
3. Click `Turbine`
4. Swipe up to set.

### CUSTOMIZE
Now that you got the basic layout, customize it how you please! If you don't use Google+ replace it!

{{ site.data.hss.ending }}

[Facets]: http://goo.gl/XHmbrk
[Squatch Icons]: http://goo.gl/Rsxayq
[Action Launcher]: http://goo.gl/6UvYxY
