# bgiesing.github.io - Brandon Giesing's Personal Site/Blog
## POWERED BY
1. [Jekyll][1]
2. [GitHub Pages][2]
3. [Thinny][3]

[1]: http://www.jekyllrb.com/
[2]: http://pages.github.com
[3]: https://github.com/camporez/Thinny
