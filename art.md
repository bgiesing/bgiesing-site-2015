---
layout: page
title: Art
permalink: /art/
published: true
---

I sometimes dabble in art. It is a fun part-time hobby of mine.

## Wallpapers
One of the things I do is make wallpapers like this:
![Polygonal Forest Wallpaper](https://lh6.googleusercontent.com/sOmeZH_GTaofrmoFEF7k--hGSuH2HBvuDAjtjeP7598=s0 "Polygonal Forest Wallpaper")

## Cartoons
I also make cartoon drawings like the below image:
![Dakko Warner](https://lh4.googleusercontent.com/KZD4KQZdCqXzeSgy0sR8RuYtAEbW-wy6oibvhI0dJzw=s0 "Dakko Warner")

This is an original character I made (based on the cartoon Animaniacs) named Dakko.

You can see more of my art at my [Google+](http://google.com/+BrandonGiesing)!

## Music
I also have made some music from MIDIs. Check them out on my SoundCloud page.


Check a sample based on the Powerhouse music made by Raymond Scott!

<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/48373637&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false">&nbsp;</iframe>
